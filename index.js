const app = require('./app')
require('./db')

const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Server running... ${PORT}`)
})
